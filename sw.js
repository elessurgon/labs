let cacheName = "prog8"
// let caching = [
//     './images/*',
//     'error.html',
//     'index.html',
//     'main.js',
//     'manifest.json',
//     'success.html',
//     'sw.js'
// ]


// ./ caches everything inside public folder
// for cutom caching change above
// for anyone copying from this remove all comments

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(cacheName).then(function(cache) {
          return cache.addAll(
            //   cahcing
            [
              './'
            ]
          );
        })
      );
});

self.addEventListener('activate', event => console.log("sw is activated",event));

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(event.request).then(function (response) {
            return response || fetch(event.request).then(function(response) {
              cache.put(event.request, response.clone());
              return response;
            });
          });
        })
      );
});