package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.FileObserver;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btn,remove;
    int flag=0;

    Fragment1 firstfragment;
    Fragment2 secondfragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn=(Button) findViewById(R.id.btn);
        remove=(Button) findViewById(R.id.remove);
        firstfragment=new Fragment1();
        secondfragment= new Fragment2();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.framelayout,firstfragment);
        ft.commit();
        flag=1;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft= fm.beginTransaction();
                if(flag==1){
                    ft.replace(R.id.framelayout,secondfragment);
                    flag=2;
                }
                else {
                    ft.replace(R.id.framelayout,firstfragment);
                    flag=1;
                }
                ft.commit();
            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft= fm.beginTransaction();
                if(flag==1){
                    ft.remove(firstfragment).commit();
                }else {
                    ft.remove(secondfragment).commit();
                }
            }
        });
    }
}